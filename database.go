package main

import (
	"fmt"
	"log"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"encoding/json"
)

var db *sql.DB

func InitDB(source string) {
	var err error
	
	// Open up our database connection.
    db, err = sql.Open("mysql", source)
	
	// if there is an error opening the connection, handle it
    if err != nil {
		log.Printf("Error while opening connection")
        panic(err.Error())
    }
	
	if err = db.Ping(); err != nil {
		log.Printf("Error while pinging db")
        panic(err.Error())
    }
}

func db_GetProducts() (Products, error) {
	rows, err := db.Query("SELECT * FROM t_products")
	defer rows.Close()
	
	if err != nil {
		log.Printf("Error while querying")
        return nil, err
    }
	
	products := make(Products, 0)
    for rows.Next() {
		var cat_id int
		
        product := new(Product)
        
		err := rows.Scan(&product.Id, &product.Name, &product.Description, &cat_id, &product.ImageLink, &product.Price, &product.Available, &product.Stock, &product.Denomination)
        if err != nil {
			log.Printf("Error while scanning : %s",rows)
            return nil, err
        }
		
		product.Category, err = db_GetProductCategory(cat_id)
		if err != nil {
			log.Printf("Error while getting product category : %s",rows)
            return nil, err
        }
		
        products = append(products, product)
    }
	
    if err = rows.Err(); err != nil {
		log.Printf("Error while processing rows")
        return nil, err
    }
	
    return products, nil
}

func db_GetProduct(id string) (Product, error) {
	str := fmt.Sprintf("SELECT * FROM t_products WHERE p_id=%v", id)
	log.Printf(str)
	
	rows, err := db.Query(str)
	defer rows.Close()
	
	if err != nil {
		log.Printf("Error while querying")
        return Product{"","","",Category{"",""},"","",false,"",""}, err
    }
	
	var product Product
	var cat_id int
	rows.Next()
	
	if err = rows.Err(); err != nil {
		log.Printf("Error while processing rows")
        return Product{"","","",Category{"",""},"","",false,"",""}, err
    }
	
	err = rows.Scan(&product.Id, &product.Name, &product.Description, &cat_id, &product.ImageLink, &product.Price, &product.Available, &product.Stock, &product.Denomination)
	
	if err != nil {
		log.Printf("Error while scanning : %s",rows)
		return Product{"","","",Category{"",""},"","",false,"",""}, err
	}
	
	product.Category, err = db_GetProductCategory(cat_id)
	if err != nil {
		log.Printf("Error while getting product category : %s",rows)
		return Product{"","","",Category{"",""},"","",false,"",""}, err
	}
	
    return product, nil
}

func db_GetProductCategory(id int) (Category, error) {
	str := fmt.Sprintf("SELECT * FROM t_category WHERE p_id=%d", id)
	//log.Printf("Querying: %s", str)
	
	rows, err := db.Query(str)
	defer rows.Close()
	if err = rows.Err(); err != nil {
		log.Printf("Error while querying: %s", str)
        return Category{"",""}, err
    }
	
	var cat Category
	rows.Next()
	
	if err = rows.Err(); err != nil {
		log.Printf("Error while processing rows")
        return Category{"",""}, err
    }
	
	err = rows.Scan(&cat.Id, &cat.Description)
	
	if err != nil {
		log.Printf("Error while scanning : %s",rows)
		return Category{"",""}, err
	}
	
	
	return cat, nil
}

func db_GetProductCategories() (Categories, error) {
	rows, err := db.Query("SELECT * FROM t_category")
	defer rows.Close()
	
	if err != nil {
		log.Printf("Error while querying Product Categories")
        return nil, err
    }
	
	categories := make(Categories, 0)
    for rows.Next() {
        cat := new(Category)
        
		err := rows.Scan(&cat.Id, &cat.Description)
        if err != nil {
			log.Printf("Error while scanning : %s",rows)
            return nil, err
        }
		
        categories = append(categories, cat)
    }
	
    if err = rows.Err(); err != nil {
		log.Printf("Error while processing rows")
        return nil, err
    }
	
    return categories, nil
}

func db_SetProduct(prod Product) (id int, err error) {
	var result sql.Result
	
	if prod.Category.Id == "" {
		prod.Category.Id = "0"
	}
	
	tempint, err := prod.Category.Id.Int64()
	
	if err != nil {
		log.Printf("Error while parsing Category ID")
		return 0, err
	}
	
	if  tempint == 0 {
		result, err := db.Exec(
			"INSERT INTO t_category (p_id, p_description) VALUES (NULL, ?)",
			prod.Category.Description,
		)
		
		if err != nil {
			log.Printf("Error while inserting category")
			return 0, err
		}
		
		cat_id, err := result.LastInsertId()
		
		if err != nil {
			log.Printf("Error while getting last inserted ID category")
			return 0, err
		}
		
		prod.Category.Id = json.Number(fmt.Sprintf("%v",cat_id))
	}
	
	tempint, err = prod.Id.Int64()
	
	if tempint == 0 {
		result, err = db.Exec(
			"INSERT INTO t_products (p_id, p_name, p_description, p_category, p_image_id, p_price, p_available, p_stock, p_denomination) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)",
			"NULL",
			prod.Name,
			prod.Description,
			prod.Category.Id,
			prod.ImageLink,
			prod.Price,
			prod.Available,
			prod.Stock,
			prod.Denomination,
		)
	} else {
		querystr := fmt.Sprintf("UPDATE t_products SET ")
		if prod.Name != "" {
			querystr += fmt.Sprintf("p_name=\"%v\",",prod.Name)
		}
		if prod.Description != "" {
			querystr += fmt.Sprintf("p_description=\"%v\",",prod.Description)
		}
		if prod.Category.Id != "" {
			querystr += fmt.Sprintf("p_category=\"%v\",",prod.Category.Id)
		}
		if prod.ImageLink != "" {
			querystr += fmt.Sprintf("p_image_id=\"%v\",",prod.ImageLink)
		}
		if prod.Price != "" {
			querystr += fmt.Sprintf("p_price=\"%v\",",prod.Price)
		}
		querystr += fmt.Sprintf("p_available=\"%v\",",prod.Available)
		if prod.Stock != "" {
			querystr += fmt.Sprintf("p_stock=\"%v\",",prod.Stock)
		}
		if prod.Denomination != "" {
			querystr += fmt.Sprintf("p_denomination=\"%v\",",prod.Denomination)
		}
		querystr = querystr[0:len(querystr)-1]
		querystr += fmt.Sprintf(" WHERE p_id=\"%v\"",prod.Id)
		
		result, err = db.Exec(querystr)
	}
	
	prod_id, err := result.LastInsertId()
	
	if err != nil {
		log.Printf("Error while inserting products")
		return 0, err
	}
	
	if err != nil {
		log.Printf("Error while getting last inserted ID product")
		return 0, err
	}
	
	return int(prod_id), nil
}

func db_DeleteProduct(id string) error {
	_, err := db.Exec(
		"DELETE FROM `t_products` WHERE p_id=?",
		id,
	)
	
	return err
}