package main

import (
	"fmt"
    "log"
    "net/http"
	"github.com/gorilla/handlers"
)

func main() {

    router := NewRouter()
	
	allowedHeaders := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"})
    allowedOrigins := handlers.AllowedOrigins([]string{"*"})
    allowedMethods := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"})
	
	fmt.Println("Go MySQL Tutorial")
	
	InitDB("admin:admin@tcp(127.0.0.1:3306)/db_shop")
    
	WebDriver_Init()
	
    log.Fatal(http.ListenAndServe(":8080", handlers.CORS(allowedHeaders, allowedOrigins, allowedMethods)(router)))
	
	// defer the close till after the main function has finished
    // executing 
    defer db.Close()
}