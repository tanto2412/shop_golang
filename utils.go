package main

import (
	"encoding/base64"
	"time"
	"os"
    "encoding/json"
    "net/http"
	"log"
	"strings"
	"io/ioutil"
	// "fmt"
)

func routineSearchTokopedia(ch chan Items, searchKeyword string) {
	log.Printf("Create routine search Tokopedia: %v", searchKeyword)
	items, next, err := WebDriver_SearchTokopedia(searchKeyword)
	
	ch <- Items{
		"tokopedia",
		items,
		next,
	}
	
	if err != nil {
		log.Printf("Error Search Tokopedia: %v", err)
	}
}

func routineSearchBukalapak(ch chan Items, searchKeyword string) {
	log.Printf("Create routine search Bukalapak: %v", searchKeyword)
	items, next, err := WebDriver_SearchBukalapak(searchKeyword)
	
	ch <- Items{
		"bukalapak",
		items,
		next,
	}
	
	if err != nil {
		log.Printf("Error Search Bukalapak: %v", err)
	}
}

func LoadSessionCookiesTokopedia() (Cookies) {
	var sessioncookies Cookies
	
	jsonFile, _ := os.Open("savedcookiestokopedia.json")
	loadedsessioncookies, _ := ioutil.ReadAll(jsonFile)
	json.Unmarshal(loadedsessioncookies, &sessioncookies)
	
	return sessioncookies
}

func LoadSessionCookiesBukalapak() (Cookies) {
	var sessioncookies Cookies
	
	jsonFile, _ := os.Open("savedcookiesbukalapak.json")
	loadedsessioncookies, _ := ioutil.ReadAll(jsonFile)
	json.Unmarshal(loadedsessioncookies, &sessioncookies)
	
	return sessioncookies
}

func StoreSessionCookiesTokopedia(sessioncookies Cookies) {
	savedsessioncookies, _ := json.Marshal(sessioncookies)
	err := ioutil.WriteFile("savedcookiestokopedia.json", savedsessioncookies, 0644)
	if err != nil {
		panic(err)
	}
}

func StoreSessionCookiesBukalapak(sessioncookies Cookies) {
	savedsessioncookies, _ := json.Marshal(sessioncookies)
	err := ioutil.WriteFile("savedcookiesbukalapak.json", savedsessioncookies, 0644)
	if err != nil {
		panic(err)
	}
}

func GetImageLink(base64image string) (string, error) {
	offset1 := strings.Index(string(base64image), "/")
	offset2 := strings.Index(string(base64image), ";")
	
	imgtype := string(base64image)[offset1+1:offset2]
	
	coI := strings.Index(string(base64image), ",")
	rawImage := string(base64image)[coI+1:]
		
	img, err := base64.StdEncoding.DecodeString(rawImage)
	
	if err != nil {
        return "", err
    }
	
	t := time.Now()
	imgName := t.Format("20060102150405")+"."+imgtype
	imgPath := "F:\\Programming\\GoWorkspace\\images\\"+imgName
	
	f, err := os.Create(imgPath)

	if err != nil {
        return "", err
    }
	
	defer f.Close()
		
	_, err = f.Write(img)
	
	if err != nil {
        return "", err
    }
	
	imgPath = "http://asem.tplinkdns.com:8080/images/"+imgName
	
	return imgPath, nil
}

func SendResponse(w http.ResponseWriter, resp interface{}) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
    w.WriteHeader(http.StatusOK)
	
	if err := json.NewEncoder(w).Encode(resp); err != nil {
        Panic(w, err, http.StatusInternalServerError)
    }
}

func Panic(w http.ResponseWriter, err error, status int) {
	response := struct {
		Diag		Diagnostic 	`json:"diagnostic"`
	} {
		Diagnostic{Status: status, Error_description: err.Error()},
	}
	
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
    w.WriteHeader(status)
	
	if err := json.NewEncoder(w).Encode(response); err != nil {
        panic(err)
    }
	
	panic(err)
}