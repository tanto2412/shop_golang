package main

import (
	// "os"
	"fmt"
    "github.com/tebeka/selenium"
	"github.com/tebeka/selenium/chrome"
	"strings"
	"log"
	// "time"
)

type Cookies []selenium.Cookie

const (
	// These paths will be different on your system.
	seleniumPath		= "F:/selenium-server-standalone-3.14.0.jar"
	chromeDriverPath	= "F:/chromedriver"
)	

var wdTokopedia, wdBukalapak selenium.WebDriver

func WebDriver_InitTokopedia() {
	var err error
	
	caps2 := selenium.Capabilities{"browserName": "chrome"}
	var chromeCaps chrome.Capabilities
	chromeCaps.Path = "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe"
	chromeCaps.Args = append(chromeCaps.Args, "start-maximized")
	chromeCaps.Args = append(chromeCaps.Args, "disable-gpu")
	chromeCaps.Args = append(chromeCaps.Args, "headless")
	chromeCaps.Args = append(chromeCaps.Args, "log-level=3")
	chromeCaps.Args = append(chromeCaps.Args, "disable-infobars")
	chromeCaps.Args = append(chromeCaps.Args, "disable-logging")
	chromeCaps.Args = append(chromeCaps.Args, "disable-login-animations")
	chromeCaps.Args = append(chromeCaps.Args, "disable-notifications")
	chromeCaps.Args = append(chromeCaps.Args, "disable-default-apps")
	chromeCaps.Prefs = make(map[string]interface{})
	chromeCaps.Prefs["profile.managed_default_content_settings.images"] = 2
	chromeCaps.Prefs["disk-cache-size"] = 4096
	caps2.AddChrome(chromeCaps)
	
	// caps := selenium.Capabilities{"browserName": "chrome"}
	wdTokopedia, err = selenium.NewRemote(caps2, "http://127.0.0.1:63000/wd/hub")
	if err != nil {
		log.Printf("Error creating chromedriver: %v",err)
	}
	// defer wdTokopedia.Quit()

	err = wdTokopedia.Get("https://www.tokopedia.com/") 
	if err != nil {
		panic(err)
	}

	log.Printf("Finished creating chromedriver for Tokopedia")
	sessioncookies := LoadSessionCookiesTokopedia()
	
	if (sessioncookies == nil) {
		if err := wdTokopedia.Get("https://www.tokopedia.com/login"); err != nil {
			panic(err)
		}
		
		a, err := wdTokopedia.FindElement(selenium.ByID, "email")
		if err != nil {
			panic(err)
		}
		a.SendKeys("tanto2412@gmail.com")
		a, err = wdTokopedia.FindElement(selenium.ByID, "password")
		if err != nil {
			panic(err)
		}
		a.SendKeys("sapiterbang")
		a.Submit()
	} else {
		for _, cookie := range sessioncookies {
			if err := wdTokopedia.AddCookie(&cookie); err != nil {
				panic(err)
			}
		}
	}
	
	cookies, err := wdTokopedia.GetCookies()
	if err != nil {
		panic(err)
	}

	sessioncookies = cookies
	StoreSessionCookiesTokopedia(sessioncookies)
}

func WebDriver_InitBukalapak() {
	var err error
	
	caps2 := selenium.Capabilities{"browserName": "chrome"}
	var chromeCaps chrome.Capabilities
	chromeCaps.Path = "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe"
	chromeCaps.Args = append(chromeCaps.Args, "start-maximized")
	chromeCaps.Args = append(chromeCaps.Args, "disable-gpu")
	chromeCaps.Args = append(chromeCaps.Args, "headless")
	chromeCaps.Args = append(chromeCaps.Args, "log-level=3")
	chromeCaps.Args = append(chromeCaps.Args, "disable-infobars")
	chromeCaps.Args = append(chromeCaps.Args, "disable-logging")
	chromeCaps.Args = append(chromeCaps.Args, "disable-login-animations")
	chromeCaps.Args = append(chromeCaps.Args, "disable-notifications")
	chromeCaps.Args = append(chromeCaps.Args, "disable-default-apps")
	chromeCaps.Prefs = make(map[string]interface{})
	chromeCaps.Prefs["profile.managed_default_content_settings.images"] = 2
	chromeCaps.Prefs["disk-cache-size"] = 4096
	caps2.AddChrome(chromeCaps)
	
	// caps := selenium.Capabilities{"browserName": "chrome"}
	wdBukalapak, err = selenium.NewRemote(caps2, "http://127.0.0.1:63001/wd/hub")
	if err != nil {
		log.Printf("Error creating chromedriver: %v",err)
	}
	// defer wdBukalapak.Quit()
	
	if err := wdBukalapak.Get("https://www.bukalapak.com/"); err != nil {
		panic(err)
	}
	
	log.Printf("Finished creating chromedriver for Bukalapak")
	sessioncookies := LoadSessionCookiesBukalapak()
	
	if (sessioncookies == nil) {
		if err := wdBukalapak.Get("https://www.bukalapak.com/login"); err != nil {
			panic(err)
		}
		
		a, err := wdBukalapak.FindElement(selenium.ByID, "user_session_username")
		if err != nil {
			panic(err)
		}
		a.SendKeys("tanto2412@gmail.com")
		a, err = wdBukalapak.FindElement(selenium.ByID, "user_session_password")
		if err != nil {
			panic(err)
		}
		a.SendKeys("sapiterbang")
		a.Submit()
	} else {
		for _, cookie := range sessioncookies {
			if err := wdBukalapak.AddCookie(&cookie); err != nil {
				panic(err)
			}
		}
	}
	
	cookies, err := wdBukalapak.GetCookies()
	if err != nil {
		panic(err)
	}

	sessioncookies = cookies
	StoreSessionCookiesBukalapak(sessioncookies)
}

func WebDriver_Init() {
	opts := []selenium.ServiceOption{// Specify the path to GeckoDriver in order to use Firefox.
		//selenium.Output(os.Stderr),            // Output debug information to STDERR.
	}
	
	
	_, err := selenium.NewChromeDriverService(chromeDriverPath, 63000, opts...)
	if err != nil {
		panic(err) // panic is used only as an example and is not otherwise recommended.
	}
	selenium.SetDebug(false)
	_, err = selenium.NewChromeDriverService(chromeDriverPath, 63001, opts...)
	if err != nil {
		panic(err) // panic is used only as an example and is not otherwise recommended.
	}
	// defer service.Stop()
	selenium.SetDebug(false)
	go WebDriver_InitTokopedia()
	go WebDriver_InitBukalapak()
	
}

func WebDriver_SearchTokopedia(keyword string) ([]*Item, bool, error){
	searchURL := fmt.Sprintf("https://www.tokopedia.com/manage-product-new.pl?sort=1&keyword=%v", keyword)
	nextPage := false
	wdTokopedia.Get(searchURL)
	
	cookies, err := wdTokopedia.GetCookies()
	if err != nil {
		return nil, nextPage, err
	}

	sessioncookies := cookies
	StoreSessionCookiesTokopedia(sessioncookies)
	
	FindElementsCondition := func(wdTokopedia selenium.WebDriver) (bool, error) {
		elements, err := wdTokopedia.FindElements(selenium.ByCSSSelector, ".break-link.fs-13.fw-600.el")
		if len(elements) == 0 {
			return false, err
		} else {
			return true, nil
		}
	}
	err = wdTokopedia.WaitWithTimeout(FindElementsCondition, 10000000000)
	if err != nil {
		return nil, nextPage, err
	}
	
	itemElements, err := wdTokopedia.FindElements(selenium.ByCSSSelector, ".moveable.ui-draggable.ui-draggable-handle.ui-droppable")
	// itemElements, err := wdTokopedia.FindElements(selenium.ByCSSSelector, ".break-link.fs-13.fw-600.el")
	if err != nil {
		return nil, nextPage, err
	}
	
	items := make([]*Item, 0)
	for _, item := range itemElements {
		itemIDString, err := item.GetAttribute("id")
		if err != nil {
			return nil, nextPage, err
		}
		
		tempString := strings.Split(itemIDString, "-")
		itemID := tempString[len(tempString)-1]
		
		itemNameElement, err := item.FindElement(selenium.ByCSSSelector, ".break-link.fs-13.fw-600.el")
		if err != nil {
			return nil, nextPage, err
		}
		
		itemName, err := itemNameElement.Text()
		if err != nil {
			return nil, nextPage, err
		}
		
		itemPriceElement, err := item.FindElement(selenium.ByCSSSelector, ".pull-left.price-input.price-input-idr.el")
		if err != nil {
			return nil, nextPage, err
		}
		
		itemPrice, err := itemPriceElement.GetAttribute("value")
		if err != nil {
			return nil, nextPage, err
		}
		
		item := Item {
			itemID,
			itemName,
			itemPrice,
		}
		items = append(items, &item)
	}
	// log.Printf(items)
	_, err = wdTokopedia.FindElement(selenium.ByLinkText, "\xbb")
	if err == nil {
		nextPage = true
	}
	return items, nextPage, nil
}

func WebDriver_SearchBukalapak(keyword string) ([]*Item, bool, error){	
	searchURL := fmt.Sprintf("https://www.bukalapak.com/my_products/stocked?search%%5Bkeywords%%5D=%v&search%%5Bper_page%%5D=30", keyword)
	nextPage := false
	wdBukalapak.Get(searchURL)
	
	cookies, err := wdBukalapak.GetCookies()
	if err != nil {
		return nil, nextPage, err
	}

	sessioncookies := cookies
	StoreSessionCookiesBukalapak(sessioncookies)
	
	FindElementsCondition := func(wdBukalapak selenium.WebDriver) (bool, error) {
		elements, err := wdBukalapak.FindElements(selenium.ByCSSSelector, ".c-product__name")
		if len(elements) == 0 {
			return false, err
		} else {
			return true, nil
		}
	}
	err = wdBukalapak.WaitWithTimeout(FindElementsCondition, 10000000000)
	if err != nil {
		return nil, nextPage, err
	}
	
	itemElements, err := wdBukalapak.FindElements(selenium.ByCSSSelector, ".c-card.c-card--list")
	if err != nil {
		return nil, nextPage, err
	}

	items := make([]*Item, 0)
	for _, item := range itemElements {
		itemNameElement, err := item.FindElement(selenium.ByCSSSelector, ".c-product__name")
		if err != nil {
			return nil, nextPage, err
		}
		
		itemName, err := itemNameElement.Text()
		if err != nil {
			return nil, nextPage, err
		}

		itemPriceElement, err := item.FindElement(selenium.ByID, "new_product_price")
		if err != nil {
			return nil, nextPage, err
		}
		
		itemPrice, err := itemPriceElement.GetAttribute("value")
		if err != nil {
			return nil, nextPage, err
		}
		
		itemIDString, err := itemPriceElement.GetAttribute("data-field")
		if err != nil {
			return nil, nextPage, err
		}
		
		tempString := strings.Split(itemIDString,"-")
		itemID := tempString[len(tempString)-1]
		
		item := Item {
			itemID,
			itemName,
			itemPrice,
		}
		items = append(items, &item)
	}
	// log.Printf(items)
	nextButtons, _ := wdBukalapak.FindElements(selenium.ByCSSSelector, ".c-pagination__btn")
	if nextButtons != nil {
		for _, element := range nextButtons {
			nextButton, _ := element.FindElement(selenium.ByCSSSelector, ".c-icon.c-icon--chevron-right")
			if nextButton != nil {
				nextPage = true
				break
			}
		}
	}
	return items, nextPage, err
}

func WebDriver_UpdateTokopedia(id string, price string) error {
	// updateURL := fmt.Sprintf("https://www.tokopedia.com/manage-product-new.pl?sort=1&keyword=%v", keyword)
	updateString := fmt.Sprintf("product-%v",id)
	itemElement, err := wdTokopedia.FindElement(selenium.ByID, updateString)
	if err != nil {
		return err
	}
	
	itemPriceElement, err := itemElement.FindElement(selenium.ByCSSSelector, ".pull-left.price-input.price-input-idr.el")
	if err != nil {
		return err
	}
	
	itemPriceElement.Clear()
	itemPriceElement.SendKeys(price)
	
	randomClick, err := itemElement.FindElement(selenium.ByName, "opt_currency")
	if err != nil {
		return err
	}
	
	randomClick.Click()
	
	return nil
}

func WebDriver_UpdateBukalapak(id string, price string) error {
	// updateURL := fmt.Sprintf("https://www.tokopedia.com/manage-product-new.pl?sort=1&keyword=%v", keyword)
	updateString := fmt.Sprintf("product_mini_edit_%v",id)
	itemElement, err := wdBukalapak.FindElement(selenium.ByID, updateString)
	if err != nil {
		return err
	}
	
	itemPriceElement, err := itemElement.FindElement(selenium.ByID, "new_product_price")
	if err != nil {
		return err
	}
	
	itemPriceElement.Click()
	wdBukalapak.KeyDown(selenium.ControlKey)
	wdBukalapak.KeyDown("a")
	wdBukalapak.KeyUp(selenium.ControlKey)
	wdBukalapak.KeyUp("a")
	itemPriceElement.SendKeys(price)
	
	randomClick, err := wdBukalapak.FindElement(selenium.ByCSSSelector, ".c-inp__ctx.u-txt--small")
	if err != nil {
		return err
	}
	
	randomClick.Click()
	
	return nil
}