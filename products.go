package main

import (
	"encoding/json"
)

/*type Todo struct {
    Name      string    `json:"name"`
    Completed bool      `json:"completed"`
    Due       time.Time `json:"due"`
}*/

type Diagnostic struct {
	Status				int		`json:"status"`
	Error_description	string	`json:"error_description"`
}

/*type Category struct {
	Id				int			`json:"id"`
	Description		string		`json:"description"`
}*/

type Category struct {
	Id				json.Number `json:"id"`
	Description		string		`json:"description"`
}

/*type Product struct {
	Id				int			`json:"id"`
	Name			string      `json:"name"`
	Description		string      `json:"description"`
	Category		Category    `json:"category"`
	ImageLink		string      `json:"imagelink"`
	Price			int         `json:"price"`
	Available		bool        `json:"available"`
	Stock			int         `json:"stock"`
	Denomination	string      `json:"denomination"`
}*/

type Product struct {
	Id				json.Number `json:"id"`
	Name			string      `json:"name"`
	Description		string      `json:"description"`
	Category		Category    `json:"category"`
	ImageLink		string      `json:"imagelink"`
	Price			json.Number `json:"price"`
	Available		bool        `json:"available"`
	Stock			json.Number `json:"stock"`
	Denomination	string      `json:"denomination"`
}

type Products []*Product

type Categories []*Category

type Packet struct {
	SenderID	json.Number `json:"senderid"`
	ReceiverID	json.Number `json:"receiverid"`
	Message		string		`json:"message"`
} 

type Items struct {
	Shop		string		`json:"shop"`
	Items		[]*Item		`json:"items"`
	NextPage	bool		`json:"next"`
}

type Item struct {
	ID			string	`json:"id"`
	Name		string	`json:"name"`
	Price		string	`json:"price"`
}

type MixItems struct {
	Tokopedia	[]*string	`json:"tokopedia"`
	Bukalapak	[]*string	`json:"bukalapak"`
}
//type Todos []Todo