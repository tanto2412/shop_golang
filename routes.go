package main

import (
    "net/http"
)

type Route struct {
    Name        string
    Method      string
    Pattern     string
    HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes{
	Route{
		"GetProducts",
		"GET",
		"/products",
		h_Products,
	},
	Route{
		"Product",
		"GET",
		"/product/{id}",
		h_Product,
	},
	Route{
		"AddProducts",
		"POST",
		"/addproducts",
		h_AddProducts,
	},
	Route{
		"UpdateProducts",
		"PUT",
		"/updateproducts",
		h_UpdateProducts,
	},
	Route{
		"DeleteProducts",
		"DELETE",
		"/deleteproducts/{path}",
		h_DeleteProducts,
	},
	Route{
		"UploadImage",
		"POST",
		"/uploadimage",
		h_UploadImage,
	},
	Route{
		"Images",
		"GET",
		"/images/{path}",
		h_GetImages,
	},
	Route{
		"GetProductCategories",
		"GET",
		"/productcategories",
		h_ProductCategories,
	},
	Route{
		"Search",
		"GET",
		"/search",
		h_Search,
	},
	Route{
		"Update",
		"GET",
		"/update",
		h_Update,
	},
	Route{
		"SendMessage",
		"POST",
		"/sendmessage",
		h_SendMessage,
	},
}