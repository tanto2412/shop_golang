package main

import (
    "encoding/json"
    "net/http"
	"io"
	"os"
	"log"
	"github.com/gorilla/websocket"
	"fmt"
	"errors"
	"strings"
)

func h_Products(w http.ResponseWriter, r *http.Request) {
	var prods Products
	var err error
	
	prods, err = db_GetProducts()
	
	if err != nil {
        Panic(w, err, http.StatusInternalServerError)
    }
	
	response := struct {
		Diag		Diagnostic 	`json:"diagnostic"`
		Prods		Products	`json:"products"`
	} {
		Diagnostic{Status: 200, Error_description: "OK"},
		prods,
	}
	
	SendResponse(w, response)
}

func h_Product(w http.ResponseWriter, r *http.Request) {
	vars := GetPath(r)
	
	product, err := db_GetProduct(vars["id"])
	
	if err != nil {
		Panic(w, err, http.StatusInternalServerError)
	}
	
	response := struct {
		Diag		Diagnostic 	`json:"diagnostic"`
		Prod		Product		`json:"product"`
	} {
		Diagnostic{Status: 200, Error_description: "OK"},
		product,
	}
	
	SendResponse(w, response)
}

func h_AddProducts(w http.ResponseWriter, r *http.Request) {
	var prod Product
	
	log.Printf("Content Length: %v", r.ContentLength);
	
	//body, err := ioutil.ReadAll(io.LimitReader(r.Body, 10485760))
	body := make([]byte, r.ContentLength)
	_, err := io.ReadFull(r.Body, body)
    if err != nil {
        Panic(w, err, http.StatusBadRequest)
    }
	
	if err := r.Body.Close(); err != nil {
        Panic(w, err, http.StatusInternalServerError)
    }
	
	if err := json.Unmarshal(body, &prod); err != nil {
        Panic(w, err, http.StatusUnprocessableEntity)
    }
	
	log.Printf("Product: %+v", prod);
		
	prod.ImageLink, err = GetImageLink(prod.ImageLink)
	
	prod_id, err := db_SetProduct(prod)
	
	if err != nil {
        Panic(w, err, http.StatusInternalServerError)
    }
	
	response := struct {
		Diag		Diagnostic 	`json:"diagnostic"`
		Id			int			`json:"id"`
	} {
		Diagnostic{Status: 200, Error_description: "OK"},
		prod_id,
	}
	
	SendResponse(w, response)
}

func h_UpdateProducts(w http.ResponseWriter, r *http.Request) {
	var prod Product
	
	log.Printf("Content Length: %v", r.ContentLength)
	
	//body, err := ioutil.ReadAll(io.LimitReader(r.Body, 10485760))
	body := make([]byte, r.ContentLength)
	_, err := io.ReadFull(r.Body, body)
    if err != nil {
        Panic(w, err, http.StatusBadRequest)
    }

	log.Printf("%s",body)
	
	if err := r.Body.Close(); err != nil {
        Panic(w, err, http.StatusInternalServerError)
    }

	if err := json.Unmarshal(body, &prod); err != nil {
        Panic(w, err, http.StatusUnprocessableEntity)
    }

	if prod.ImageLink != "" {
		prod.ImageLink, err = GetImageLink(prod.ImageLink)
		
		if err != nil {
			Panic(w, err, http.StatusBadRequest)
		}
	}

	prod_id, err := db_SetProduct(prod)
	
	if err != nil {
        Panic(w, err, http.StatusInternalServerError)
    }

	response := struct {
		Diag		Diagnostic 	`json:"diagnostic"`
		Id			int			`json:"id"`
	} {
		Diagnostic{Status: 200, Error_description: "OK"},
		prod_id,
	}
	
	SendResponse(w, response)
}

func h_DeleteProducts(w http.ResponseWriter, r *http.Request) {
	vars := GetPath(r)
	
	err := db_DeleteProduct(vars["path"])
	
	if err != nil {
		Panic(w, err, http.StatusInternalServerError)
	}
	
	response := struct {
		Diag		Diagnostic 	`json:"diagnostic"`
	} {
		Diagnostic{Status: 200, Error_description: "OK"},
	}
	
	SendResponse(w, response)
}

func h_UploadImage(w http.ResponseWriter, r *http.Request) {
	img := struct {
		Image string `json:"image"`
	} {
		"",
	}
	
	body := make([]byte, r.ContentLength)
	_, err := io.ReadFull(r.Body, body)
    if err != nil {
        Panic(w, err, http.StatusBadRequest)
    }
	
	if err := json.Unmarshal(body, &img); err != nil {
        Panic(w, err, http.StatusUnprocessableEntity)
    }
	
	imgLink, err := GetImageLink(img.Image)
	if err != nil {
        Panic(w, err, http.StatusNotFound)
    }	
	
	response := struct {
		Diag		Diagnostic 	`json:"diagnostic"`
		ImageLink	string		`json:"imagelink"`
	} {
		Diagnostic{Status: 200, Error_description: "OK"},
		imgLink,
	}
	
	SendResponse(w, response)
}

func h_GetImages(w http.ResponseWriter, r *http.Request) {
	//log.Printf("%+v",w)
	vars := GetPath(r)
	
	path := "F:\\Programming\\GoWorkspace\\images\\" + vars["path"]
	
	img, err := os.Open(path)
	if err != nil {
        Panic(w, err, http.StatusNotFound) // perhaps handle this nicer
    }
	
	fi, err := img.Stat()
	if err != nil {
		Panic(w, err, http.StatusNotFound)
	}
	
	defer img.Close()
	w.Header().Set("Content-Type", "image/jpeg") // <-- set the content-type header
	w.Header().Set("accept-ranges", "bytes")
	w.Header().Set("access-control-allow-methods", "GET, OPTIONS")
	w.Header().Set("access-control-allow-origin", "*")
	w.Header().Set("age", "42")
	w.Header().Set("cache-control", "public, max-age=31536000")
	w.Header().Set("content-length", fmt.Sprintf("%v",fi.Size()))
	w.Header().Set("server", "cat factory 1.0")
	w.Header().Set("status","200")
    io.Copy(w, img)
}

func h_ProductCategories(w http.ResponseWriter, r *http.Request) {
	var cats Categories
	var err error
	
	cats, err = db_GetProductCategories()
	
	if err != nil {
        Panic(w, err, http.StatusInternalServerError)
    }
	
	response := struct {
		Diag		Diagnostic 	`json:"diagnostic"`
		Cats		Categories	`json:"categories"`
	} {
		Diagnostic{Status: 200, Error_description: "OK"},
		cats,
	}
	
	SendResponse(w, response)
}

/*--------------------------------------------------------------------------------------------*/

var clients = make(map[int]*websocket.Conn)

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

var packets = make(chan Packet) 

/*func h_Login(w http.ResponseWriter, r *http.Request) {
	logininfo := struct {
		SenderID	json.Number `json:"senderid"`
	} {
		"0",
	}
	log.Printf("%v",r.Header)

	// Upgrade initial GET request to a websocket
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		Panic(w, err, http.StatusInternalServerError)
	}
	
	defer ws.Close()
	
	err = ws.ReadJSON(&logininfo)
    if err != nil {
        Panic(w, err, http.StatusBadRequest)
    }
	
	senderID, err := logininfo.SenderID.Int64()
	if err != nil {
		Panic(w, err, http.StatusInternalServerError)
    }
	
	clients[int(senderID)] = ws
	
	log.Printf("clients: %v", clients)
}*/

func h_Search(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	
	searchKeyword := r.Form["keyword"][0]
	
	chTokopedia := make(chan Items)
	chBukalapak := make(chan Items)
	
	go routineSearchTokopedia(chTokopedia, searchKeyword)
	go routineSearchBukalapak(chBukalapak, searchKeyword)

	itemsTokopedia := <- chTokopedia
	itemsBukalapak := <- chBukalapak
	
	var products []Items
	products = append(products, itemsTokopedia)
	products = append(products, itemsBukalapak)

	response := struct {
		Diag		Diagnostic 	`json:"diagnostic"`
		Products	[]Items		`json:"products"`
	} {
		Diagnostic{Status: 200, Error_description: "OK"},
		products,
	}
	
	SendResponse(w, response)
}

func h_Update(w http.ResponseWriter, r *http.Request) {
	var err error
	
	r.ParseForm()
	
	shop := r.Form["shop"][0]
	itemID := r.Form["id"][0]
	itemPrice := r.Form["price"][0]
	
	if strings.ToUpper(shop) == "TOKOPEDIA" {
		err = WebDriver_UpdateTokopedia(itemID, itemPrice)
	} else {
		if strings.ToUpper(shop) == "BUKALAPAK" {
			err = WebDriver_UpdateBukalapak(itemID, itemPrice)
		} else {
			err = errors.New("Shop not recognized")
		}
	}
	
	if err != nil {
		Panic(w, err, http.StatusInternalServerError)
	}

	response := struct {
		Diag		Diagnostic 	`json:"diagnostic"`
	} {
		Diagnostic{Status: 200, Error_description: "OK"},
	}
	
	SendResponse(w, response)
}

func h_SendMessage(w http.ResponseWriter, r *http.Request) {
	var packet Packet
	
	body := make([]byte, r.ContentLength)
	_, err := io.ReadFull(r.Body, body)
    if err != nil {
        Panic(w, err, http.StatusBadRequest)
    }
	
	if err := json.Unmarshal(body, &packet); err != nil {
        Panic(w, err, http.StatusUnprocessableEntity)
    }
	
	packets <- packet
}

func h_DeliverMessage() {
	for {
		// Grab the next message from the channel
		packet := <- packets
		
		receiverID, err := packet.ReceiverID.Int64()
		if err != nil {
			panic(err)
		}
		
		if ws, ok := clients[int(receiverID)]; ok == true {
			err := ws.WriteJSON(packet)
			if err != nil {
				ws.Close()
				delete(clients, int(receiverID))
				panic(err)
			}
		}
	}
}
